import 'package:flutter/material.dart';
import './control.dart';

class Output extends StatefulWidget{
  @override
  _OutputState createState() => _OutputState();
}

class _OutputState extends State<Output>{
  String msg = 'Fajar Ali Akbar';

  void _changeText() {
    setState(() {
      if (msg.startsWith('F')) {
        msg = 'Oke FajarFajar';
      } else if (msg.startsWith('A')) {
        msg = 'Fajar Ali Akbar';
      }else{
        msg = 'Ali Fajar Akbarr';
      }
      });
    }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text('Username : ', style: new TextStyle(fontSize:30.0),),
          Control(msg),
          RaisedButton(child: Text("Acak Username",style: new TextStyle( color: Colors.white),),color: Colors.black,onPressed:_changeText,),
        ],
      ),
    );
  }
}